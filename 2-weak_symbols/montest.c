#include "runtime.h"
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] )
{
    RUNTIME_init( 10 );

    INSERT_TASK_helloworld( "It's a wonderfull world" );

    RUNTIME_fini();
}
