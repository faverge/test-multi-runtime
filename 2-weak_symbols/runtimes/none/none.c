#include "runtime.h"
#include <stdio.h>

#pragma weak RUNTIME_init = NONE_init
#pragma weak RUNTIME_fini = NONE_fini
#pragma weak INSERT_TASK_helloworld = NONE_TASK_helloworld

void NONE_init( int param ) {
    fprintf( stderr, "NONE: Runtime initialized\n" );
}
    
void NONE_fini()
{
    fprintf( stderr, "NONE: Runtime finalized\n" );
}

void NONE_TASK_helloworld( char *welcome_msg )
{
    fprintf( stderr, "NONE: %s\n", welcome_msg );
}

