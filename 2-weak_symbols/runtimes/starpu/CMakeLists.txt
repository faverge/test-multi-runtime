add_library( runtime_starpu
  starpu.c
  )

target_link_libraries( runtime_starpu runtime )
