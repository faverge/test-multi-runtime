#ifndef _montest_dlopen_h_
#define _montest_dlopen_h_

/**
 *  Routines to handle dlopen
 */
#ifdef __cplusplus
extern "C" {
#endif

int montest_dlopen();
void montest_dlclose();

#ifdef __cplusplus
}
#endif

#endif /* _montest_dlopen_h_ */
