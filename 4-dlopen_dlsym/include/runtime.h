#ifndef _runtime_h_
#define _runtime_h_

/* functions prototypes defined in runtimes libs */
void RUNTIME_init( int param );
void RUNTIME_fini();
void INSERT_TASK_helloworld( char *welcome_msg );

/* functions prototypes used in montest, declared here as pointers, link to the
real function done with dlopen */
void (*MONTEST_RUNTIME_init)( int param );
void (*MONTEST_RUNTIME_fini)();
void (*MONTEST_INSERT_TASK_helloworld)( char *welcome_msg );

#endif
