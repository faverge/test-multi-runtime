#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include "runtime.h"

/**
 *  Global data
 */
/* dlopen handle */
static void *montest_dlopen_handle = NULL;

/**
 * @brief Macro to call dlsym for every runtime symbols
 */
#define RUNTIME_DLSYM( name )                                           \
    *(void**)(&MONTEST_##name) = dlsym( montest_dlopen_handle, #name ); \
    if ( (error = dlerror()) != NULL ){                                 \
         fprintf(stderr, "dlsym has failed\n");                         \
         return EXIT_FAILURE;                                           \
    }

/**
 * @brief Macro to affect MONTEST_INSERT_TASK|RUNTIME functions to
 * INSERT_TASK|RUNTIME ones. Usefull when not using dlopen for runtime symbols
 * *i.e.* when there is only one runtime interface enabled at configuration.
 */
#define RUNTIME_AFFECT( name ) MONTEST_##name = name;

/**
 *
 *  @brief montest_dlopen - Load the runtime symbols given by the
 *  __MONTEST_RUNTIME_LIBRARY__ environment variable.
 *
 * The runtime chosen must be enabled during the configuration.
 *
 ******************************************************************************
 *
 */
int montest_dlopen( int runtime )
{
#if defined(MONTEST_DLOPEN)
    char *error;

	fprintf( stderr, "runtime is %d\n", runtime );
	switch( runtime ){
	case 1:
	    montest_dlopen_handle = dlopen("libruntime_parsec.so", RTLD_NOW );
	    break;
	case 2:
	    montest_dlopen_handle = dlopen("libruntime_quark.so", RTLD_NOW );
	    break;
	case 3:
	default:
	    montest_dlopen_handle = dlopen("libruntime_starpu.so", RTLD_NOW );
	}
	if (!montest_dlopen_handle) {
	    fprintf(stderr, "%s\n", dlerror());
	    return EXIT_FAILURE;
	}

    /* define symbols with dlsym */
    RUNTIME_DLSYM( RUNTIME_init           )
    RUNTIME_DLSYM( RUNTIME_fini           )
    RUNTIME_DLSYM( INSERT_TASK_helloworld )
#else
    /* define symbols, use the available (linked) interface */
    RUNTIME_AFFECT( RUNTIME_init           )
    RUNTIME_AFFECT( RUNTIME_fini           )
    RUNTIME_AFFECT( INSERT_TASK_helloworld )
#endif
    return EXIT_SUCCESS;
}

void montest_dlclose()
{
#if defined(MONTEST_DLOPEN)
    dlclose(montest_dlopen_handle);
#endif
}