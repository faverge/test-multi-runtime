#include <stdio.h>
#include <stdlib.h>
#include "runtime.h"
#include "montest_dlopen.h"

int main( int argc, char *argv[] )
{

    int runtime = 0;

    if ( argc > 1 ) {
	    runtime = atoi( argv[1] );
    }

    int err = montest_dlopen( runtime );

    if (err != EXIT_SUCCESS){
        return EXIT_FAILURE;
    }

    MONTEST_RUNTIME_init( 10 );

    MONTEST_INSERT_TASK_helloworld( "It's a wonderfull world" );

    MONTEST_RUNTIME_fini();

    montest_dlclose();

    return EXIT_SUCCESS;
}
