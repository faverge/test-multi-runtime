This repository aims at testing multiple solutions that could be put
in place to enable multiple runtimes with the chameleon library within
a single build.

The ` template` directory holds a base version that aims at mimicking
the Chameleon code structure.

Each solution is proposed in the `X-description` directory. Please use
your prefered diff tool (meld, kompare, diff, ...) to check the
differences between the different solutions and the impact it may have
on the Chameleon code.


# Solution 1 - Multiple build (1-multiple_binaries)

The first solution consist in working only on the cmake infrastructure
to enable multiple build of the binaries: one for each enebled runtime
systems.

We would end up with a serie of binaries answering the following
pattern:
     `chameleon_[sdcz]testing_[parsec,starpu,quark,openmp]`

This means, that we should remove the `libchameleon`, such that each
`libchameleon_XXXX` includes both the content of the actual
`libchameleon` and the actual `libchameleon_XXXX` with `XXXX` one of
the runtime.

This solution allows to build everything, and just duplicated the
binaries at link time. However it not possible to choose at runtime,
and the libray is now runtime specific.

# Solution 2 - Weak symbols

The second solution defines a library as an empty interface with weak
symbols that performs nothing. Note that this could be replaced by a
default runtime.

The behavior is then to call the empty library by default, unless a
specified runtime is preloaded at runtime.

~~~sh
> ./montest
~~~
~~~txt
NONE: Runtime initialized
NONE: It's a wonderfull world
NONE: Runtime finalized
~~~
~~~sh
> LD_PRELOAD=./runtimes/parsec/libruntime_parsec.so ./montest
~~~
~~~txt
PARSEC: Runtime initialized
PARSEC: It's a wonderfull world
PARSEC: Runtime finalized
~~~
~~~sh
> LD_PRELOAD=./runtimes/starpu/libruntime_starpu.so ./montest
~~~
~~~txt
STARPU: Runtime initialized
STARPU: It's a wonderfull world
STARPU: Runtime finalized
~~~

If anyone knows a solution to preload from the binary through `dlopen`
or any other means. The solution is welcome :).

One of the default here, is that it does not work with static
libraries. A default runtime must then be chosen when static builds are
required.

# Solution 3 - Add a fake wrapper library and use dlopen

This solution adds a layer to call the correct library at runtime.
At intialization, the correct runtime is loaded with dlopen, and all
symbols are solved to be called via a thin interface layer that could
be automatically generated from the `.h` interface files.

This has the small advantage compared with solution 4 that we do not
have to change the naming everywhere. But need to see if it has an
impact on performances.

In the build directory:
~~~sh
> cmake .. -DCMAKE_INSTALL_PREFIX=$PWD/install && make install
> ./montest
> ./install/bin/montest
~~~
~~~txt
runtime is 0
STARPU: Runtime initialized
STARPU: It's a wonderfull world
STARPU: Runtime finalized
~~~
~~~sh
> ./montest 1
~~~
~~~txt
runtime is 1
PARSEC: Runtime initialized
PARSEC: It's a wonderfull world
PARSEC: Runtime finalized
~~~
~~~sh
> ./montest 2
~~~
~~~txt
runtime is 2
QUARK: Runtime initialized
QUARK: It's a wonderfull world
QUARK: Runtime finalized
~~~
~~~sh
> ./montest 3
~~~
~~~txt
runtime is 3
STARPU: Runtime initialized
STARPU: It's a wonderfull world
STARPU: Runtime finalized
~~~

One of the default here, is the requirement of setting the LD_LIBRARY_PATH correctly. It would be nice if we could set directly these values within the DT_RUNPATH of the binary.

# Solution 4 - dlopen/dlsym

One build allows to use one of the 3 `libruntime_xxxx` runtime
libraries (parsec, quark, starpu) at runtime.

This is done with the
[dlopen/dlsym](https://linux.die.net/man/3/dlopen) mechanism. We build
all activated runtime libraries and the library to be used is
dynamically loaded at runtime depending on the choice of the user.

If several runtimes are enabled at configuration we force to build
chameleon shared (dynamic) libraries. If no runtime are enabled we
send a message error. If only one runtime library is enabled dlopen is
not used and we fall in the *normal* situation and both shared an
static mode are available.

```
git clone https://gitlab.inria.fr/faverge/test-multi-runtime.git
cd test-multi-runtime/
cd 4-dlopen_dlsym/
mkdir build && cd build
cmake .. # default only starpu is enabled
make
./montest
cmake .. -DRUNTIME_PARSEC=ON -DRUNTIME_QUARK=ON -DRUNTIME_STARPU=ON
make # all runtimes are enabled
export LD_LIBRARY_PATH=$PWD/runtimes/parsec:$PWD/runtimes/quark:$PWD/runtimes/starpu:$LD_LIBRARY_PATH
./montest 0
./montest 1
./montest 2
```

# Difference solutions 3 and 4

## 3 in brief

New library *none* with definition of a runtime interface that just
calls the proper runtime function, e.g. dlsym _INSERT_TASK_helloworld
-> INSERT_TASK_helloworld at initialization using the user's library
option (0, 1, 2) then definition, in the *none* library, of
INSERT_TASK_helloworld calling _INSERT_TASK_helloworld.

## 4 in brief

No additional library but two different names for runtime functions,
the ones used in the application **MONTEST_INSERT_TASK_** and the ones
used inside runtime libraries **INSERT_TASK_**. At initialization
dlsym MONTEST_INSERT_TASK_helloworld -> INSERT_TASK_helloworld using
the same options mechanism as 3.

## Conclusion

3 is nothing more than 4 but with a trick to avoid having two classes
of names for runtime functions.