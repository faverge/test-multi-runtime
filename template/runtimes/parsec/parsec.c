#include "runtime.h"
#include <stdio.h>


void RUNTIME_init( int param ) {
    fprintf( stderr, "PARSEC: Runtime initialized\n" );
}
    
void RUNTIME_fini()
{
    fprintf( stderr, "PARSEC: Runtime finalized\n" );
}

void INSERT_TASK_helloworld( char *welcome_msg )
{
    fprintf( stderr, "PARSEC: %s\n", welcome_msg );
}

