#include "runtime.h"
#include <stdio.h>
#include <stdlib.h>

#include <dlfcn.h>

void *_runtime_handle = NULL;

void (*_RUNTIME_init)( int ) = NULL;
void (*_RUNTIME_fini)() = NULL;
void (*_INSERT_TASK_helloworld)( char * ) = NULL;

void RUNTIME_init( int param )
{
    char *error;

    if ( !_runtime_handle ) {
	fprintf( stderr, "runtime is %d\n", param );
	switch( param ){
	case 1:
	    _runtime_handle = dlopen("libruntime_parsec.so", RTLD_LAZY );
	    break;
	case 2:
	    _runtime_handle = dlopen("libruntime_quark.so", RTLD_LAZY );
	    break;
	case 3:
	default:
	    _runtime_handle = dlopen("libruntime_starpu.so", RTLD_LAZY );
	}
	if (!_runtime_handle) {
	    fprintf(stderr, "%s\n", dlerror());
	    exit(EXIT_FAILURE);
	}

	dlerror();    /* Clear any existing error */

	_RUNTIME_init = dlsym( _runtime_handle, "RUNTIME_init" );
	error = dlerror();
	if (error != NULL) {
	    fprintf(stderr, "%s\n", error);
	    exit(EXIT_FAILURE);
	}

	_RUNTIME_fini = dlsym( _runtime_handle, "RUNTIME_fini" );
	error = dlerror();
	if (error != NULL) {
	    fprintf(stderr, "%s\n", error);
	    exit(EXIT_FAILURE);
	}

	_INSERT_TASK_helloworld = dlsym( _runtime_handle, "INSERT_TASK_helloworld" );
	error = dlerror();
	if (error != NULL) {
	    fprintf(stderr, "%s\n", error);
	    exit(EXIT_FAILURE);
	}
    }

    _RUNTIME_init( param );
}

void RUNTIME_fini( ) {
    _RUNTIME_fini( );
    if ( _runtime_handle ) {
	dlclose( _runtime_handle );
	_runtime_handle = NULL;
    }
}

void INSERT_TASK_helloworld( char *msg ) {
    return _INSERT_TASK_helloworld( msg );
}
