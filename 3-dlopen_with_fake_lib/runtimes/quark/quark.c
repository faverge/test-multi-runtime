#include "runtime.h"
#include <stdio.h>


void RUNTIME_init( int param ) {
    fprintf( stderr, "QUARK: Runtime initialized\n" );
}
    
void RUNTIME_fini()
{
    fprintf( stderr, "QUARK: Runtime finalized\n" );
}

void INSERT_TASK_helloworld( char *welcome_msg )
{
    fprintf( stderr, "QUARK: %s\n", welcome_msg );
}

