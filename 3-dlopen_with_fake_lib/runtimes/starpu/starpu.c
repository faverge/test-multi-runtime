#include "runtime.h"
#include <stdio.h>


void RUNTIME_init( int param ) {
    fprintf( stderr, "STARPU: Runtime initialized\n" );
}
    
void RUNTIME_fini()
{
    fprintf( stderr, "STARPU: Runtime finalized\n" );
}

void INSERT_TASK_helloworld( char *welcome_msg )
{
    fprintf( stderr, "STARPU: %s\n", welcome_msg );
}

