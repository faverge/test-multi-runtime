#include "runtime.h"
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] )
{
    int runtime = 0;
    
    if ( argc > 1 ) {
	runtime = atoi( argv[1] );
    }

    RUNTIME_init( runtime );

    INSERT_TASK_helloworld( "It's a wonderfull world" );

    RUNTIME_fini();
}
